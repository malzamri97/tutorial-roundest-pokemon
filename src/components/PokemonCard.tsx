import { Center, Image, Text } from "@chakra-ui/react";

const PokemonCard = ({ pokemon }) => {
	return (
		<Center border="1px" minW={["100px", "200px"]} minH="200px" bg="red.800" flexDir="column">
			{/* {first ?? ""} */}
			<Image
				src={pokemon.data?.sprites.front_default || ""}
				alt={pokemon.data?.species.name}
				w="full"
			/>
			<Text textTransform="capitalize" mt="-4" mb="3">
				{pokemon.data?.species.name}
			</Text>
		</Center>
	);
};

export default PokemonCard;
