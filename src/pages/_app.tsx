import { useEffect, useState } from "react";
import type { AppProps } from "next/app";
import { Center, ChakraProvider, Flex, Spinner } from "@chakra-ui/react";

function MyApp({ Component, pageProps }: AppProps) {
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		setIsLoading(false);
	}, []);

	if (isLoading)
		return (
			<ChakraProvider>
				<Center minH="100vh" minW="100vw" bg="gray.800" color="white">
					<Spinner />
				</Center>
			</ChakraProvider>
		);

	return (
		<ChakraProvider>
			<Flex minH="100vh" minW="100vw" bg="gray.800" color="white">
				<Component {...pageProps} />
			</Flex>
		</ChakraProvider>
	);
}

import { withTRPC } from "@trpc/next";
import type { AppRouter } from "@/backend/router";

export default withTRPC<AppRouter>({
	config({ ctx }) {
		/**
		 * If you want to use SSR, you need to use the server's full URL
		 * @link https://trpc.io/docs/ssr
		 */
		const url = process.env.VERCEL_URL
			? `https://${process.env.VERCEL_URL}/api/trpc`
			: "http://localhost:3000/api/trpc";

		return {
			url,
			/**
			 * @link https://react-query.tanstack.com/reference/QueryClient
			 */
			// queryClientConfig: { defaultOptions: { queries: { staleTime: 60 } } },
		};
	},
	/**
	 * @link https://trpc.io/docs/ssr
	 */
	ssr: true,
})(MyApp);
