import { useMemo, useState } from "react";
import { getOptionsForVote } from "@/utils/getRandomPokemon";
import { trpc } from "@/utils/trpc";

// Chakra
import { Center, Container, Heading, Image, Stack, Text } from "@chakra-ui/react";
import PokemonCard from "@/components/PokemonCard";

const Home = () => {
	const [ids, setIds] = useState(() => getOptionsForVote());

	const [first, second] = ids;

	const firstPokemon = trpc.useQuery(["get-pokemon-by-id", { id: first }]);
	const secondPokemon = trpc.useQuery(["get-pokemon-by-id", { id: second }]);

	console.log(firstPokemon.data);

	if (firstPokemon.isLoading || secondPokemon.isLoading) return null;

	return (
		<Container
			// bg="red.100"
			maxW="6xl"
			display="flex"
			justifyContent="center"
			alignItems="center"
			flexDirection="column"
		>
			<Heading py="10">Which pokemon is the roundest?</Heading>
			<Stack direction="row" alignItems="center" border="1px" p="2">
				<PokemonCard pokemon={firstPokemon} />
				<Text>vs</Text>
				<PokemonCard pokemon={secondPokemon} />
			</Stack>
		</Container>
	);
};

export default Home;
